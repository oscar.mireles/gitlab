// eslint-disable-next-line import/prefer-default-export
export const hasLicenses = state => state.licenses.length > 0;
